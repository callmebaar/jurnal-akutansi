<?php $__env->startSection('title', 'Form Edit Data Jurnal'); ?>

<?php $__env->startSection('container'); ?>
<div class="col-7">
  <h1 class="mt-1">Form edit data Jurnal</h1>
  <?php if($errors->any()): ?>
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
      <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
    </div>
  <?php endif; ?>
  <form method="post" action="/jurnal/update/<?php echo e($jurnal->id); ?>">
    <?php echo e(csrf_field()); ?>

    <div class="form-group">
      <label for="wkt_jurnal">Waktu</label>
      <input type="date" class="form-control <?php $__errorArgs = ['wkt_jurnal'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="wkt_jurnal" placeholder="masukkan waktu..." name="wkt_jurnal" value="<?php echo e($jurnal->wkt_jurnal); ?>">
    </div>
    <div class="form-group">
      <label for="keterangan">Keterangan</label>
      <input type="text" class="form-control <?php $__errorArgs = ['keterangan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="keterangan" placeholder="masukkan keterangan..." name="keterangan" value="<?php echo e($jurnal->keterangan); ?>">
    </div>
    <div class="form-group">
      <label for="total">Total</label>
      <input class="form-control <?php $__errorArgs = ['total'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="total" name="total" placeholder="masukkan total..." type="int" value="<?php echo e($jurnal->total); ?>">
    </div>
    <div>
      <a href="/jurnal" class="btn btn-secondary">KEMBALI</a>
      <button type="submit" class="btn btn-primary float-right">EDIT</button>
    </div>
  </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/jurnal/edit.blade.php ENDPATH**/ ?>