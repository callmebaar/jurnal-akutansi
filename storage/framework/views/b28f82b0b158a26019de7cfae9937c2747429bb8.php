<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title><?php echo $__env->yieldContent('title'); ?></title>

    <style>
      body{
        background-image: url('blue.png');
        background-repeat: no-repeat;
        background-size:cover;
        font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
      }
      .sign-up-btn{
        width : 100px;
        background-color: darkturquoise;
        border-style: hidden;
      }
      .logo{
        margin-left : 20px;
        width : 160px;
        height : 35px;
      }
      .register-form{
        background-color : darkslategrey;
        margin : 10px;
        padding : 20px;
        width : 400px;
        padding-bottom : 50px;
      }
      .world{
        position: absolute;
        margin-left: 550px;
        width : 700px;
        height : 380px;
        margin-top : 40px;
        margin-bottom : 30px;
      }
      .ikonName{
        position: absolute;
        top: 113px;
        left : 20px;
      }
      .ikonEmail{
        position: absolute;
        top: 191px;
        left : 20px;
      }
      .ikonPassword{
        position: absolute;
        top: 269px;
        left : 20px;
      }
      .ikonPasswordConfirmation{
        position: absolute;
        top: 347px;
        left : 20px;
      }
      .inputWithIcon input[type=text]{
        padding-left: 40px;
      }
      .inputWithIcon input[type=email]{
        padding-left: 40px;
      }
      .inputWithIcon input[type=password]{
        padding-left: 40px;
      }
      .inputWithIcon.inputIconBg i{
        background-color: black;
        color : darkturquoise;
        padding : 11px 10px;
        border-radius: 4px 0 0 4px;
      }
    </style>

  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <img src="logo-white.png" class="logo">
      </ul>
      <form class="form-inline" method="POST" action="<?php echo e(route('login')); ?>">
        <?php echo e(csrf_field()); ?>

        <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Masukkan alamat email..." required autofocus>
        <input type="password" id="inputPassword" class="form-control ml-2" name="password" placeholder="Masukkan password..." required>
        <button class="btn btn-outline-primary btn-sm ml-2" type="submit">Sign in</button>
      </form>
    </div>
    </nav>
    <?php echo $__env->yieldContent('container'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/layout/all.blade.php ENDPATH**/ ?>