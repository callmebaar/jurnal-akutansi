<?php $__env->startSection('title', 'Buku Besar'); ?>

<?php $__env->startSection('container'); ?>
<div class="container">
    <h1>Buku Besar</h1>
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr align="center">
                <th>#</th>
                <th>Waktu</th>
                <th>Keterangan</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jrl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $jrl->rekening; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rkg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td><?php echo e($jrl->wkt_jurnal); ?></td>
                <td><?php echo e($jrl->keterangan); ?></td>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td><?php echo e($rkg->saldo); ?></td>
                            </tr>
                        </tbody>
                    <table>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/bukubesar/index.blade.php ENDPATH**/ ?>