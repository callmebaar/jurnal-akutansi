<?php $__env->startSection('title', 'Login'); ?>

<?php $__env->startSection('container'); ?>
<br>
<div class="">
  <img src="world-white.png" class="world">
  <form class="col-5 register-form" method = "POST" action="<?php echo e(route('register')); ?>">
    <?php echo e(csrf_field()); ?>

    <h1 class="h3 mb-1 font-weight-bold text-white" align="center">Register</h1><br>
    <label for="inputName" class="text-white">Nama</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-user-circle icon ikonName"></i>
      <input type="text" name="name" id="inputName" class="form-control mb-2 <?php echo e($errors->has('name') ? 'is-invalid' : ''); ?>" placeholder="Masukkan nama..." value="<?php echo e(old('name')); ?>" required autofocus>
    </div>
      <?php if($errors->has('name')): ?>
    <div class="invalid-feedback">
      <?php echo e($errors->first('name')); ?>

    </div>
    <?php endif; ?>
    <label for="inputEmail" class="text-white">Alamat email</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-envelope icon ikonEmail"></i>
      <input type="email" name="email" id="inputEmail" class="form-control mb-2 <?php echo e($errors->has('email') ? 'is-invalid' : ''); ?>" placeholder="Masukkan alamat email..." value="<?php echo e(old('email')); ?>" required autofocus>
    </div>
    <?php if($errors->has('email')): ?>
      <div class="invalid-feedback">
          <?php echo e($errors->first('email')); ?>

      </div>
    <?php endif; ?>
    <label for="inputPassword" class="text-white">Password</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-key icon ikonPassword"></i>
      <input type="password" name="password" id="inputPassword" class="form-control mb-2 <?php echo e($errors->has('password') ? 'is-invalid' : ''); ?>" placeholder="Masukkan password..." required>
    </div>
    <?php if($errors->has('password')): ?>
    <div class="invalid-feedback">
      <?php echo e($errors->first('password')); ?>

    </div>
    <?php endif; ?>
    <label for="inputPassword" class="text-white">Password Confirmation</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-key icon ikonPasswordConfirmation"></i>
      <input type="password" name="password_confirmation" id="inputPassword" class="form-control mb-3 <?php echo e($errors->has('password_confirmation') ? 'is-invalid' : ''); ?>" placeholder="Masukkan password..." required>
    </div>
    <?php if($errors->has('password_confirmation')): ?>
    <div class="invalid-feedback">
      <?php echo e($errors->first('password_confirmation')); ?>

    </div>
    <?php endif; ?>
    <button class="btn btn-primary sign-up-button float-right sign-up-btn" type="submit">Sign up</button>
  </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/all', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/login.blade.php ENDPATH**/ ?>