<?php $__env->startSection('title', 'Daftar Jurnal'); ?>

<?php $__env->startSection('container'); ?>
<div class="container">
    <h1 class="mt-1">Jurnal</h1>
    <?php if(session('status')): ?>
        <div class="alert alert-primary">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr align="center">
                <th>#</th>
                <th>Waktu</th>
                <th width="100px">Keterangan</th>
                <th>Item</th>
                <th>Total</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jrl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td align="center">
                    <p>
                        Dibuat pada : <br><br>
                        <?php echo e(\Carbon\Carbon::parse($jrl->wkt_jurnal)->format('l, d/M/Y')); ?><br><br>
                        <?php echo e(\Carbon\Carbon::parse($jrl->wkt_jurnal)->diffForHumans()); ?>

                    </p>
                </td>
                <td align="center"><?php echo e($jrl->keterangan); ?></td>
                <td>
                    <?php if(count($jrl->rekening)): ?>
                        <table border="1" align="center">
                            <thead class="thead-dark">
                                <tr align="center">
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Saldo</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $jrl->rekening; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rkg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($rkg->nama); ?></td>
                                        <td><?php echo e($rkg->saldo); ?></td>
                                        <td>
                                            <a href="/rekening/edit/<?php echo e($rkg->id); ?>" class="badge badge-info">Edit</a>
                                            <form>
                                                <?php echo csrf_field(); ?>
                                                <a href="/rekening/delete/<?php echo e($rkg->id); ?>" class="badge badge-danger" onclick="return confirm('Apakah anda ingin menghapus item <?php echo e($rkg->nama); ?> ?')">Hapus</a>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <center><b>Item kosong</b></center>
                    <?php endif; ?>
                </td>
                <td align="center"><?php echo e($jrl->total); ?></td>
                <td align="center">
                    <a href="/rekening/tambah" class="btn btn-info">Tambah Item</a>
                    <a href="/jurnal/edit/<?php echo e($jrl->id); ?>" class="btn btn-success">Edit</a>
                    <a href="/jurnal/delete/<?php echo e($jrl->id); ?>" class="btn btn-danger mt-1" onclick="return confirm('Apakah anda ingin menghapus jurnal <?php echo e($jrl->keterangan); ?> ?')">Hapus</a>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <a href="/jurnal/tambah" class="btn btn-primary mb-3 float-right">Tambah Data</a>
    <a href="/jurnal" class="btn btn-secondary mr-2 mb-3 float-right">Tampilkan Semua</a>
    <?php echo $jurnal->render(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/jurnal/index.blade.php ENDPATH**/ ?>