<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error | 404 Not Found</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .dua{
            position:absolute;
            margin-left: 38%;
            margin-top: 0px;
            width: 320px;
            height: 250px;
        }
        h1{
            text-align: center;
            font-size: 60px;
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            font-weight: lighter;
            margin-top:90px;
        }
        h6{
            text-align: center;
            font-size: 20px;
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            font-weight: lighter;
            margin-top:20%;
        }
        a{
            margin-left:46%;
            margin-top:20px;
        }
    </style>
</head>
<body>
    <h1>404 | Not Found</h1>
    <img src="notfound.png" class="dua">
    <h6>Halaman yang anda cari tidak tersedia.</h6>
    <a class="btn btn-outline-secondary" href="/login">KEMBALI</a>
</body>
</html><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/error404.blade.php ENDPATH**/ ?>