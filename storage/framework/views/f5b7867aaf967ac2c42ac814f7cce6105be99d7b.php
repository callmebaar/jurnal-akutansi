<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style>
      li{
        display: inline-block;
      }

      li a{
        text-decoration: none;
        color:#fff;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-weight: lighter;
        font-size: 20px;
        padding: 0 20px;
      }

      .cool-link::after{
        content: '';
        display: block;
        width: 0;
        height: 2px;
        background: dodgerblue;
        transition: width .3s
      }

      .cool-link:hover::after{
        width:100%;
        transition: width .3s
      }
    </style>

    <title><?php echo $__env->yieldContent('title'); ?></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active cool-link">
          <a class="nav-link" href="/home">Jurnal Akutansi</a>
        </li>
        <li class="nav-item active cool-link">
            <a class="nav-link" href="/rekening">Rekening</a>
        </li>
        <li class="nav-item active cool-link">
            <a class="nav-link" href="/jurnal">Jurnal</a>
        </li>
        <li class="nav-item active cool-link">
            <a class="nav-link" href="/bukubesar">Buku Besar</a>
        </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input name="cari" class="form-control mr-sm-2" type="search" placeholder="Cari..." aria-label="Search">
        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Cari</button>
        </form>
      </div>
    </nav>
    <?php echo $__env->yieldContent('container'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/layout/main.blade.php ENDPATH**/ ?>