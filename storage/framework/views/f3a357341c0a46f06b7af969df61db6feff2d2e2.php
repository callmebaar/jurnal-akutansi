<?php $__env->startSection('title', 'Form Tambah Data Rekening'); ?>

<?php $__env->startSection('container'); ?>
<div class="col-7">
  <h1 class="mt-1">Form tambah data Rekening</h1>
  <?php if($errors->any()): ?>
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
      <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
    </div>
  <?php endif; ?>
  <form method="post" action="/rekening/store">
    <?php echo e(csrf_field()); ?>

    <div class="form-group">
      <label for="keterangan">Keterangan</label>
      <select name="jurnal_id" class="form-control <?php $__errorArgs = ['jurnal_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
        <option value="">-- Pilih keterangan --</option>
        <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jrn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($jrn->id); ?>"><?php echo e($jrn->id); ?>. <?php echo e($jrn->keterangan); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
    </div>
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control <?php $__errorArgs = ['nama'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="nama" placeholder="masukkan nama..." name="nama">
    </div>
    <div class="form-group">
      <label for="saldo">Saldo</label>
      <input class="form-control <?php $__errorArgs = ['saldo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="saldo" name="saldo" placeholder="masukkan saldo..." type="int">
    </div>
    <div>
      <a href="/rekening" class="btn btn-secondary">KEMBALI</a>
      <button type="submit" class="btn btn-primary float-right">TAMBAH</button>
    </div>
  </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/rekening/tambah.blade.php ENDPATH**/ ?>