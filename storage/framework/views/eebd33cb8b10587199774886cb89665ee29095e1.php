<?php $__env->startSection('title', 'Daftar Rekening'); ?>

<?php $__env->startSection('container'); ?>

<div class="col-7">
    <h1 class="mb-5 mt-2">Rekening</h1>
    <?php if(session('status')): ?>
        <div class="alert alert-primary">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Keterangan</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $rekening; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rkg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <th><?php echo e($loop->iteration); ?></th>
                <td><?php echo e($rkg->keterangan); ?></td>
                <td><?php echo e($rkg->nama); ?></td>
                <td>
                    <a href="/rekening/edit/<?php echo e($rkg->id); ?>" class="btn btn-success">Edit</a>
                    <a href="/rekening/delete/<?php echo e($rkg->id); ?>" class="btn btn-danger" onclick="return confirm('Apakah anda ingin menghapus item <?php echo e($rkg->nama); ?>?')">Hapus</a>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <a href="/rekening/tambah" class="btn btn-primary float-right ml-2">Tambah Data</a>
    <a href="/rekening" class="btn btn-secondary float-right ml-2">Tampilkan Semua</a>
    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target=".bd-example-modal-lg">Create new</button>
    <?php echo $rekening->render(); ?>

</div>
<?php $__env->stopSection(); ?>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="col-7">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    <h1 class="mt-1">Form tambah data Rekening</h1>
    <?php if($errors->any()): ?>
        <div class="alert alert-primary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        </div>
    <?php endif; ?>
    <form method="post" action="/rekening/store">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <select name="jurnal_id" class="form-control <?php $__errorArgs = ['jurnal_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
            <option value="">-- Pilih keterangan --</option>
            <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jrn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($jrn->id); ?>"><?php echo e($jrn->id); ?>. <?php echo e($jrn->keterangan); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        </div>
        <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control <?php $__errorArgs = ['nama'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="nama" placeholder="masukkan nama..." name="nama">
        </div>
        <div class="form-group">
        <label for="saldo">Saldo</label>
        <input class="form-control <?php $__errorArgs = ['saldo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="saldo" name="saldo" placeholder="masukkan saldo..." type="int">
        </div>
        <div>
        <button type="submit" class="btn btn-primary float-right mb-3">TAMBAH</button>
        </div>
    </form>
    </div>
    </div>
  </div>
</div>
<?php echo $__env->make('layout/main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\JurnalAkutansi\resources\views/rekening/index.blade.php ENDPATH**/ ?>