<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .ha1{
            text-align:center;
            margin-top:10%;
            color:darkslategrey;
            font-size: 90px;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        .nama{
            font-style:italic;
            font-weight:lighter;
        }

        li{
            display: inline-block;
        }

        li a{
            text-decoration: none;
            color:#fff;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-weight: lighter;
            font-size: 20px;
            padding: 0 20px;
        }

        .cool-link::after{
            content: '';
            display: block;
            width: 0;
            height: 2px;
            background: dodgerblue;
            transition: width .3s
        }

        .cool-link:hover::after{
            width:100%;
            transition: width .3s
        }

        li.dropdown {
            display: inline-block;
        }
    
        .dropdown:hover .dropdown-menu {
            display: block;
        }
    
        .dropdown-menu a:hover {
            color: #fff !important;
        }
    
        .dropdown-menu {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            background-color: #f9f9f9;
        }
    
        .dropdown-menu a {
            color: #3c3c3c !important;
        }
    
        .dropdown-menu a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }

        .dropdown{
            color:#fff;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active cool-link">
                    <a class="nav-link" href="/home">Jurnal Akutansi</a>
                </li>
                <li class="nav-item active cool-link">
                    <a class="nav-link" href="/rekening">Rekening</a>
                </li>
                <li class="nav-item active cool-link">
                    <a class="nav-link" href="/jurnal">Jurnal</a>
                </li>
                <li class="nav-item active cool-link">
                    <a class="nav-link" href="/bukubesar">Buku Besar</a>
                </li>
            </ul>
            <ul>
                <li class="nav-item active dropdown "><a>Hello, {{ Auth::user()->name }}</a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-item"><a href="#">Settings</a></li>
                        <li class="dropdown-item"><a href="{{ route('logout') }}">Log-out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <h1 class="ha1">Welcome, <p class="nama">{{ Auth::user()->name }}<p></h1>
</body>
</html>
    