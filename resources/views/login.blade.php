@extends('layout/all')

@section('title', 'Login')

@section('container')
<br>
<div class="">
  <img src="world-white.png" class="world">
  <form class="col-5 register-form" method = "POST" action="{{ route('register') }}">
    {{ csrf_field() }}
    <h1 class="h3 mb-1 font-weight-bold text-white" align="center">Register</h1><br>
    <label for="inputName" class="text-white">Nama</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-user-circle icon ikonName"></i>
      <input type="text" name="name" id="inputName" class="form-control mb-2 {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Masukkan nama..." value="{{ old('name') }}" required autofocus>
    </div>
      @if($errors->has('name'))
    <div class="invalid-feedback">
      {{ $errors->first('name') }}
    </div>
    @endif
    <label for="inputEmail" class="text-white">Alamat email</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-envelope icon ikonEmail"></i>
      <input type="email" name="email" id="inputEmail" class="form-control mb-2 {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Masukkan alamat email..." value="{{ old('email') }}" required autofocus>
    </div>
    @if($errors->has('email'))
      <div class="invalid-feedback">
          {{ $errors->first('email') }}
      </div>
    @endif
    <label for="inputPassword" class="text-white">Password</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-key icon ikonPassword"></i>
      <input type="password" name="password" id="inputPassword" class="form-control mb-2 {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Masukkan password..." required>
    </div>
    @if($errors->has('password'))
    <div class="invalid-feedback">
      {{ $errors->first('password') }}
    </div>
    @endif
    <label for="inputPassword" class="text-white">Password Confirmation</label>
    <div class="inputWithIcon inputIconBg">
      <i class="fa fa-key icon ikonPasswordConfirmation"></i>
      <input type="password" name="password_confirmation" id="inputPassword" class="form-control mb-3 {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" placeholder="Masukkan password..." required>
    </div>
    @if($errors->has('password_confirmation'))
    <div class="invalid-feedback">
      {{ $errors->first('password_confirmation') }}
    </div>
    @endif
    <button class="btn btn-primary sign-up-button float-right sign-up-btn" type="submit">Sign up</button>
  </form>
</div>
@endsection