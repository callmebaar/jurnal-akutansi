@extends('layout/main')

@section('title', 'Daftar Rekening')

@section('container')

<div class="col-7">
    <h1 class="mb-5 mt-2">Rekening</h1>
    @if(session('status'))
        <div class="alert alert-primary">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('status')}}
        </div>
    @endif
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Keterangan</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @foreach($rekening as $rkg)
            <tr>
                <th>{{ $loop->iteration }}</th>
                <td>{{ $rkg->keterangan }}</td>
                <td>{{ $rkg->nama }}</td>
                <td>
                    <a href="/rekening/edit/{{$rkg->id}}" class="btn btn-success">Edit</a>
                    <a href="/rekening/delete/{{$rkg->id}}" class="btn btn-danger" onclick="return confirm('Apakah anda ingin menghapus item {{ $rkg->nama }}?')">Hapus</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="/rekening/tambah" class="btn btn-primary float-right ml-2">Tambah Data</a>
    <a href="/rekening" class="btn btn-secondary float-right ml-2">Tampilkan Semua</a>
    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target=".bd-example-modal-lg">Create new</button>
    {!! $rekening->render() !!}
</div>
@endsection


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="col-7">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    <h1 class="mt-1">Form tambah data Rekening</h1>
    @if($errors->any())
        <div class="alert alert-primary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif
    <form method="post" action="/rekening/store">
        {{ csrf_field() }}
        <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <select name="jurnal_id" class="form-control @error ('jurnal_id') is-invalid @enderror">
            <option value="">-- Pilih keterangan --</option>
            @foreach($jurnal as $jrn)
            <option value="{{ $jrn->id }}">{{ $jrn->id }}. {{ $jrn->keterangan }}</option>
            @endforeach
        </select>
        </div>
        <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error ('nama') is-invalid @enderror" id="nama" placeholder="masukkan nama..." name="nama">
        </div>
        <div class="form-group">
        <label for="saldo">Saldo</label>
        <input class="form-control @error ('saldo') is-invalid @enderror" id="saldo" name="saldo" placeholder="masukkan saldo..." type="int">
        </div>
        <div>
        <button type="submit" class="btn btn-primary float-right mb-3">TAMBAH</button>
        </div>
    </form>
    </div>
    </div>
  </div>
</div>