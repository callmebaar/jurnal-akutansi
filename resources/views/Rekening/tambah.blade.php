@extends('layout/main')

@section('title', 'Form Tambah Data Rekening')

@section('container')
<div class="col-7">
  <h1 class="mt-1">Form tambah data Rekening</h1>
  @if($errors->any())
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
      <ul>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <form method="post" action="/rekening/store">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="keterangan">Keterangan</label>
      <select name="jurnal_id" class="form-control @error ('jurnal_id') is-invalid @enderror">
        <option value="">-- Pilih keterangan --</option>
        @foreach($jurnal as $jrn)
        <option value="{{ $jrn->id }}">{{ $jrn->id }}. {{ $jrn->keterangan }}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control @error ('nama') is-invalid @enderror" id="nama" placeholder="masukkan nama..." name="nama">
    </div>
    <div class="form-group">
      <label for="saldo">Saldo</label>
      <input class="form-control @error ('saldo') is-invalid @enderror" id="saldo" name="saldo" placeholder="masukkan saldo..." type="int">
    </div>
    <div>
      <a href="/rekening" class="btn btn-secondary">KEMBALI</a>
      <button type="submit" class="btn btn-primary float-right">TAMBAH</button>
    </div>
  </form>
</div>
@endsection