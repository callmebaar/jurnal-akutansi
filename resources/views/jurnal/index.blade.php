@extends('layout/main')

@section('title', 'Daftar Jurnal')

@section('container')
<div class="container">
    <h1 class="mt-1">Jurnal</h1>
    @if(session('status'))
        <div class="alert alert-primary">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('status')}}
        </div>
    @endif
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr align="center">
                <th>#</th>
                <th>Waktu</th>
                <th width="100px">Keterangan</th>
                <th>Item</th>
                <th>Total</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jurnal as $jrl)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td align="center">
                    <p>
                        Dibuat pada : <br><br>
                        {{ \Carbon\Carbon::parse($jrl->wkt_jurnal)->format('l, d/M/Y') }}<br><br>
                        {{ \Carbon\Carbon::parse($jrl->wkt_jurnal)->diffForHumans() }}
                    </p>
                </td>
                <td align="center">{{ $jrl->keterangan }}</td>
                <td>
                    @if(count($jrl->rekening))
                        <table border="1" align="center">
                            <thead class="thead-dark">
                                <tr align="center">
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Saldo</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($jrl->rekening as $rkg)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $rkg->nama }}</td>
                                        <td>{{ $rkg->saldo }}</td>
                                        <td>
                                            <a href="/rekening/edit/{{ $rkg->id }}" class="badge badge-info">Edit</a>
                                            <form>
                                                @csrf
                                                <a href="/rekening/delete/{{$rkg->id}}" class="badge badge-danger" onclick="return confirm('Apakah anda ingin menghapus item {{ $rkg->nama }} ?')">Hapus</a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <center><b>Item kosong</b></center>
                    @endif
                </td>
                <td align="center">{{ $jrl->total }}</td>
                <td align="center">
                    <a href="/rekening/tambah" class="btn btn-info">Tambah Item</a>
                    <a href="/jurnal/edit/{{$jrl->id}}" class="btn btn-success">Edit</a>
                    <a href="/jurnal/delete/{{$jrl->id}}" class="btn btn-danger mt-1" onclick="return confirm('Apakah anda ingin menghapus jurnal {{ $jrl->keterangan }} ?')">Hapus</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="/jurnal/tambah" class="btn btn-primary mb-3 float-right">Tambah Data</a>
    <a href="/jurnal" class="btn btn-secondary mr-2 mb-3 float-right">Tampilkan Semua</a>
    {!! $jurnal->render() !!}
</div>
@endsection