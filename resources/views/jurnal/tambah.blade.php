@extends('layout/main')

@section('title', 'Form Tambah Data Jurnal')

@section('container')
<div class="col-7">
  <h1 class="mt-1">Form tambah data Jurnal</h1>
  @if($errors->any())
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
      <ul>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <form method="post" action="/jurnal/store">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="wkt_jurnal">Waktu</label>
      <input class="form-control @error ('wkt_jurnal') is-invalid @enderror" id="wkt_jurnal" name="wkt_jurnal" placeholder="masukkan waktu..." type="date">
    </div>
    <div class="form-group">
      <label for="keterangan">Keterangan</label>
      <input type="text" class="form-control @error ('keterangan') is-invalid @enderror" id="keterangan" placeholder="masukkan keterangan..." name="keterangan">
    </div>
    <div class="form-group">
      <label for="total">Total</label>
      <input type="int" class="form-control @error ('total') is-invalid @enderror" id="total" placeholder="masukkan total..." name="total">
    </div>
    <div>
      <a href="/jurnal" class="btn btn-secondary">KEMBALI</a>
      <button type="submit" class="btn btn-primary float-right">TAMBAH</button>
    </div>
  </form>
</div>
@endsection