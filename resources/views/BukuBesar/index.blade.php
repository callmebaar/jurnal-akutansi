@extends('layout/main')

@section('title', 'Buku Besar')

@section('container')
<div class="container">
    <h1>Buku Besar</h1>
    <table class="table" border="1">
        <thead class="thead-dark">
            <tr align="center">
                <th>#</th>
                <th>Waktu</th>
                <th>Keterangan</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jurnal as $jrl)
            @foreach($jrl->rekening as $rkg)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $jrl->wkt_jurnal }}</td>
                <td>{{ $jrl->keterangan }}</td>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $rkg->saldo }}</td>
                            </tr>
                        </tbody>
                    <table>
                </td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
    </table>
</div>
@endsection