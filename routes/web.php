<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication

Route::get('/login', 'AuthController@getLogin')->name('login')->middleware('guest');
Route::post('/login', 'AuthController@postLogin')->middleware('guest');
Route::get('/register', 'AuthController@getRegister')->middleware('guest')->name('register');
Route::post('/register', 'AuthController@postRegister')->middleware('guest');
Route::get('/home', function () {
    return view('home');
})->middleware('auth')->name('home');
Route::get('/logout', 'AuthController@logout')->middleware('auth')->name('logout');

// Jurnal

Route::get('/jurnal', 'JurnalController@index')->middleware('auth');
Route::get('/jurnal/tambah', 'JurnalController@create')->middleware('auth');
Route::post('/jurnal/store', 'JurnalController@store');
Route::get('/jurnal/delete/{id}', 'JurnalController@destroy')->middleware('auth');
Route::get('/jurnal/edit/{id}', 'JurnalController@edit')->middleware('auth');
Route::post('/jurnal/update/{id}', 'JurnalController@update');

// Rekening

Route::get('/rekening', 'RekeningController@index')->middleware('auth');
Route::get('/rekening/tambah', 'RekeningController@create')->middleware('auth');
Route::post('/rekening/store', 'RekeningController@store');
Route::get('/rekening/delete/{id}', 'RekeningController@destroy')->middleware('auth');
Route::get('/rekening/edit/{id}', 'RekeningController@edit')->middleware('auth');
Route::post('/rekening/update/{id}', 'RekeningController@update');

// Buku Besar

Route::get('/bukubesar', 'BukuBesarController@index')->middleware('auth');