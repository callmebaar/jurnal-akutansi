<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Jurnal extends Model
{
    protected $table = "Jurnal";

    public function rekening(){
        return $this->hasMany('App\Rekening');
    }

    // public function getWktJurnalAttribute($value){
    //     return Carbon::parse($value)->format('d/m/Y');
    // }
}
