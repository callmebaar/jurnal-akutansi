<?php

namespace App\Http\Controllers;

use App\Jurnal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuBesarController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $jurnal = Jurnal::with('rekening')
            ->select('jurnal.*')->where('keterangan', 'like', '%'.$request->cari.'%')
            ->paginate(2);
        }else{
            $jurnal = Jurnal::with('rekening')
            ->paginate(2);
        }
        return view('bukubesar.index', compact('jurnal'));
    }
}
