<?php

namespace App\Http\Controllers;

use App\Rekening;
use App\Http\Requests\RekeningRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('cari')){
            $rekening = DB::table('rekening')
            ->join('jurnal', 'jurnal.id', '=', 'rekening.jurnal_id')
            ->select('rekening.*', 'jurnal.keterangan')->where('nama', 'like', '%'.$request->cari.'%')
            ->paginate();
        }else{
            $jurnal = DB::table('jurnal')->get();
            $rekening = DB::table('rekening')
            ->join('jurnal', 'jurnal.id', '=', 'rekening.jurnal_id')
            ->select('rekening.*', 'jurnal.keterangan')
            ->paginate(3);
        }
        // return view('rekening.index', ['rekening' => $rekening]);
        return view('rekening.index', compact('jurnal', 'rekening'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jurnal = DB::table('jurnal')->get();
        return view('rekening.tambah', compact('jurnal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RekeningRequest $request)
    {
        // DB::table('rekening')->insert([
        //     'nama' => $request->nama,
        //     'saldo' => $request->saldo
        // ]);
        // $request->validate([
        //     'jurnal_id' => 'required',
        //     'nama' => 'required',
        //     'saldo' => 'required|numeric'
        // ]);
        $this->validate( $request, $request->messages());
        Rekening::create($request->all());
        return redirect('/rekening')->with('status', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function show(Rekening $rekening)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rekening = Rekening::find($id);
        $jurnal = DB::table('jurnal')->get();
        return view('rekening.edit', compact('rekening', 'jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function update($id, RekeningRequest $request)
    {
        $this->validate( $request, $request->messages());
        DB::table('rekening')->where('id', $request->id)->update([
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);
        // Rekening::update($request->all());
        return redirect('/rekening')->with('status', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('rekening')->where('id', $id)->delete();
        return redirect()->back()->with('status', 'Data berhasil dihapus');
    }
}
