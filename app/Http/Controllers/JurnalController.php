<?php

namespace App\Http\Controllers;

use App\Jurnal;
use App\Http\Requests\JurnalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('cari')){
            $jurnal = Jurnal::with('rekening')
            ->select('jurnal.*')->where('keterangan', 'like', '%'.$request->cari.'%')
            ->paginate(2);
        }else{
            $jurnal = Jurnal::with('rekening')
            ->paginate(2);
        }
        return view('jurnal.index', ['jurnal' => $jurnal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jurnal.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JurnalRequest $request)
    {
        $this->validate( $request, $request->messages());
        DB::table('jurnal')->insert([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan,
            'total' => $request->total
        ]);

        return redirect('/jurnal')->with('status', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function show(Jurnal $jurnal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnal = Jurnal::find($id);
        return view('jurnal.edit', compact('jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function update($id, JurnalRequest $request)
    {
        $this->validate( $request, $request->messages());
        DB::table('jurnal')->where('id', $request->id)->update([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan,
            'total' => $request->total
        ]);
        return redirect('/jurnal')->with('status', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jurnal = Jurnal::find($id);
        $jurnal->delete();
        return redirect()->back()->with('status', 'Data berhasil dihapus');
    }
}
