<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RekeningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jurnal_id' => 'required',
            'nama' => 'required|max:30',
            'saldo' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'jurnal_id.required' => 'Keterangan tidak valid',
            'nama.required' => 'Nama harus diisi',
            'nama.max' => 'Nama tidak boleh melebihi 30 karakter',
            'saldo.required' => 'Saldo harus diisi',
            'saldo.numeric' => 'Saldo harus berupa angka'
        ];
    }
}