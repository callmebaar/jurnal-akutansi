<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JurnalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wkt_jurnal' => 'required|date',
            'keterangan' => 'required|max:30',
            'total' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'wkt_jurnal.required' => 'Waktu harus diisi',
            'wkt_jurnal.date' => 'Waktu harus berupa tanggal',
            'keterangan.required' => 'Keterangan harus diisi',
            'keterangan.max' => 'Keterangan tidak boleh melebihi 30 karakter',
            'total.required' => 'Total harus diisi',
            'total.numeric' => 'Total harus berupa angka'
        ];
    }
}